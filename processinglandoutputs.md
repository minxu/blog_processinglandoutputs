# Postprocessing of land model outputs in NE grids for ILAMB analysis

Min Xu and Forrest M. Hoffman


If land model outputs from E3SM or CESM are in a cubed sphere grid, generally denoted by ne[X]np[Y] or ne[X] (where X and Y are integers), 
the outputs need to be remapped to a regular lat-lon grid and reformatted following the CMIP convention before they can be analyzed by 
the International Land Model Benchmarking (ILAMB) python diagnostic package.  


## Conservatively remapped to a regular lat-lon grid 

Because the ILAMB package currently can only analyze the land model outputs in a one dimensional lat-lon grid including the uniform, FV, and Gaussian grids,
we need to remap the model outputs from a NE grid to a regular lat-lon grid in a conservative way.

There are two tools available.
- *ncremap* from NCO using the option "-sgs". But it requires the NCO version larger than 4.5.7  and 
the ESMF_RegridWeightGen tool that is generally included in the widely-used NCL package. 
- *conv_remap2.sh* from the github repo https://github.com/minxu74/conv_remap2. Compared with ncremap, 
it can only remap data from a NE to regular grid, but it can use any NCO versions. 

Both tools need grid description files in the SCRIP format. NE grid files in the SCRIP format can be obtained from the CESM or E3SM data input directories or servers. 
For regular grids in the SCRIP format, you can use ncks to generate them. For example:
```
ncks --rgr grd_ttl='Equi-Angular grid 180x360'#latlon=180,360#lat_typ=uni#lon_typ=grn_wst --rgr scrip=180x360_SCRIP.nc in.nc ~/foo.nc
```
It will generate a global grid in a 1 by 1 degree resolution in the SCRIP format.


Note: 
1. Since both conv_remap and ncremap use the ESMF_RegridWeightGen and its option "--user-areas", please make sure its version higher than 6.3.0. 
2. This step can skipped if the land model outputs are in a regular lat-lon grid.


## Reformatted following the CF and CMIP5 conventions

This tool uses the NCO toolkit to reformat the land outputs following the CF and CMIP5 conventions. After the conversion, 
variables in land monthly outputs are broken up into single variable time-series that are saved to files with names from their corresponding names in the CMIP5 convention.

For example, if you ran a case of ACME water cycle historical experiment from 1850-2050, the case name is *interp_20160520.A_WCYCL1850.ne30_oEC.edison.alpha6_01* and 
its results are stored in the directory */lustre/atlas1/cli106/proj-shared/mxu/ALM_ILAMB/ILAMB/ALM_WCYCL/*. Now you want to rewrote its results following CMIP conventions 
into the directory */lustre/atlas1/cli106/proj-shared/mxu/ALM_ILAMB/RESULT/* for analysis in ILAMB. Then try to run the commands as follows:

- Step 1, clone the repo:
```
git clone https://minxu@bitbucket.org/minxu/alm2ilamb_wkflow.git
```

- Step 2*, rewrite the fixed datasets including *sftlf* (land fraction) and *areacella* (grid cell area) by:
```
clm_singlevar_ts.csh --caseid interp_20160520.A_WCYCL1850.ne30_oEC.edison.alpha6_01 --centuries 18,19,20 -y 1850-2050 

          -i interp_20160520.A_WCYCL1850.ne30_oEC.edison.alpha6_01 -o /lustre/atlas1/cli106/proj-shared/mxu/ALM_ILAMB/RESULT/ 

          --experiment history --model ALM_CYCLE --numcc 6 --cmip --ilamb --addfxflds
```

- Step 3, rewrite the other varaibles:
```
clm_singlevar_ts.csh --caseid interp_20160520.A_WCYCL1850.ne30_oEC.edison.alpha6_01 --centuries 18,19,20 -y 1850-2050 

          -i interp_20160520.A_WCYCL1850.ne30_oEC.edison.alpha6_01 -o /lustre/atlas1/cli106/proj-shared/mxu/ALM_ILAMB/RESULT/ 

          --experiment history --model ALM_CYCLE --numcc 6 --cmip --ilamb
```

For details, please refer to [README](https://bitbucket.org/minxu/alm2ilamb_wkflow/src/master/README.md)

\* You can skip this step and let ILAMB calculate the land fraction and grid cell area for you.

Note: there is a Python implementation with similar functionalities in [e3sm_to_cmip](https://github.com/E3SM-Project/e3sm_to_cmip) project



## Python virtual environment or conda environment for ILAMB


A Python environment is a independent, and self-contained directory includes a Python version and a number of packages that satisfy their dependencies. 
It can be activated and deactivated to run a python application in a set of packages. It also can be shared by multiple users, so they have same environment 
settings to run a same python application.


The benefits of run a application in a Python environment are:

- self-contained directory tree
- satisfied the dependency and package versions required by specific applications
- Independent from the system installed/loaded packages
- some degrees in cross-platform portability (can be used in the machines with similar OS) 
- shared with multiple users for running a application in a same environment

The default paths of the python environment for ILAMB on the machines are:
- OLCF/Titan: /lustre/atlas1/cli106/world-shared/mxu/ilamb_venv (open to everyone)
- OLCF/Rhea: /lustre/atlas1/cli106/world-shared/mxu/ilamb_venv (open to everyone)

- NERSC/Edison: 
   - /project/projectdirs/m2467/ilamb_venv (open to project members in m2467)
   - /project/projectdirs/acme/ilamb_venv (open to project members in acme)

- NERSC/Cori:
   - /project/projectdirs/m2467/ilamb_venv (open to project members in m2467)
   - /project/projectdirs/acme/ilamb_venv (open to project members in acme)

- ORNL/Cades: /lustre/or-hydra/cades-ccsi/e4x/ilamb_venv (open to project members in CCSI)
- Others machines to be added in future

__Usage__:
 - clone the repo:
```
https://github.com/rubisco-sfa/ilamb-venv.git
```
- activate or load the virtual environment*:
```
source ilamb-venv/activate_ilamb_venv.sh
```
\* Please run bash before the above command if you are in other shells.


- run ILAMB in a serial, for example in Edison and Cori:

```
export ILAMB_ROOT=your_directory_where_the DATA_and MODELS_directories_are
cd $ILAMB_ROOT
ilamb_run --model_root MODELS --config ilamb.cfg --regions global bona tena ceam nhsa shsa euro mide nhaf shaf boas ceas seas eqas aust
```

- run ILAMB in a parallel job, for example in Edison and Cori:

```
sbatch job_ilamb_template.sh
```

The job_ilamb_template.sh
```
#!/bin/bash 
#SBATCH -q regular
#SBATCH -N 1
#SBATCH -t 08:00:00
#SBATCH -J ilamb_job
#SBATCH -o ilamb_job.o%j
#SBATCH -L SCRATCH,project

source ilamb_venv/activate_ilamb_venv.sh
export ILAMB_ROOT=/global/cscratch1/sd/minxu/ILAMB2_WCYCLE/ILAMB_ROOT
srun -n 24 ilamb-run --model_root MODELS --config ilamb.cfg --regions global bona tena ceam nhsa shsa euro mide nhaf shaf boas ceas seas eqas aust

```



### Reference
1. ILAMB website: www.ilamb.org
2. ILAMB code repo: bitbucket.org/ncollier/ilamb
3. Conda user guide: https://conda.io/docs/user-guide/index.html
4. NCO: http://nco.sourceforge.net/
5. Python virtual environment repo: https://github.com/rubisco-sfa/ilamb-venv.git
6. Conservative remapping repo: https://github.com/minxu74/conv_remap2.git
7. Reformat land outputs in CMIP5 conventions repo: https://minxu@bitbucket.org/minxu/alm2ilamb_wkflow.git
8. e3sm_to_cmip: https://github.com/E3SM-Project/e3sm_to_cmip
